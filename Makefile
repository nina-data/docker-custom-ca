IMAGES ?= alpine:latest docker:stable docker:dind
__IMAGES__ = $(subst :,_,$(IMAGES))
DEST ?= output

.PHONY: all clean
all: $(__IMAGES__)

clean:
	rm -rf output

$(__IMAGES__):%:$(DEST)/%/Dockerfile

$(DEST)/%/Dockerfile: Dockerfile
	mkdir -p $(@D)
	image=$(subst _,:,$*) envsubst < Dockerfile > $@

