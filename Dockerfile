FROM ${image}

COPY ca.crt /usr/local/share/ca-certificates/ca.crt
RUN command -v update-ca-certificates >/dev/null 2>&1 \
      || apk add --no-cache ca-certificates wget \
      && update-ca-certificates --fresh >/dev/null
